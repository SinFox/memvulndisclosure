#include <fstream>
#include <iostream>
#include <stack>
#include <map>

#include "StackManager.h"
#include "report.h"

using namespace::std;

#ifdef TARGET_IA32E
    constexpr REG INS_PTR = REG_RIP;
    constexpr REG STOP_PTR = REG_RSP;
#else
    constexpr REG INS_PTR = REG_EIP;
    constexpr REG STOP_PTR = REG_ESP;
#endif

struct RtnInfo
{
    ADDRINT Offset;
    const char* Name;
};

struct StackTop
{
    ADDRINT Address;
    ADDRINT Value;
};

struct LastStackFrame
{
    ADDRINT Offset;
    ADDRINT ADDRESS;
    ADDRINT VALUE;
};

BOOL entryPassed = false;
vector<string> rtnNames;
vector<RtnInfo> stackInfo;
vector<StackTop> stackRets;

VOID rtnEntryHandlerFull(ADDRINT offset, BOOL isEntry, ADDRINT retAddr, ADDRINT rip)
{
    if (isEntry)
    {
        entryPassed = true;
    }

    if (entryPassed && (stackRets.empty() || retAddr != stackRets.back().Address))
    {
        StackTop st;
        ADDRINT r;
        ADDRINT i = 0;
        for (; i <= 10; i++)
        {
            PIN_SafeCopy(&r, (ADDRINT*)(retAddr + i * sizeof(ADDRINT)), sizeof(ADDRINT));
            if (r == rip)
            {
                break;
            }
        }

        st.Address = retAddr + i * sizeof(ADDRINT);
        st.Value = r;

        RtnInfo ri;
        ri.Offset = offset;
        ri.Name = "";

        stackInfo.push_back(ri);
        stackRets.push_back(st);
    }
}

VOID insCallHandler(ADDRINT call_addr, ADDRINT ret_addr, ADDRINT stack_top, ADDRINT entry_addr)
{
    if (entry_addr == call_addr)
        entryPassed = true;

    if (entryPassed)
    {
        StackTop st;
        st.Address = stack_top - sizeof(ADDRINT);
        st.Value = ret_addr;

        RtnInfo ri;
        ri.Offset = call_addr;
        ri.Name = "";

        stackInfo.push_back(ri);
        stackRets.push_back(st);
    }
}

VOID insRetHandler(ADDRINT ins_addr)
{
    if (entryPassed && !stackRets.empty())
    {
        ADDRINT stack_top = stackRets.back().Address;
        ADDRINT val;
        PIN_SafeCopy(&val, (ADDRINT*)stack_top, sizeof(ADDRINT));

        /*for (ADDRINT i = stack_top - 8 * sizeof(ADDRINT); i <= stack_top + 8 * sizeof(ADDRINT); i += sizeof(ADDRINT))
        {
            ADDRINT tmp;
            PIN_SafeCopy(&tmp, (ADDRINT*)i, sizeof(ADDRINT));
            cout << hexstr(tmp) << endl;
        }*/
        // StackToStr(&stackInfo, &stackRets); 

        if (val != stackRets.back().Value)
        {
            report_so(&stackInfo, &stackRets, ins_addr, val);
        }

        stackRets.pop_back();
        if (!stackInfo.empty())
            stackInfo.pop_back();

        if (stackRets.empty())
            entryPassed = false;
    }
}

VOID rtnEntryHandler(BOOL isEntry, ADDRINT retAddr)
{
    if (isEntry)
        entryPassed = true;

    if (entryPassed)
    {
        StackTop st;
        ADDRINT v;
        PIN_SafeCopy(&v, (ADDRINT*)retAddr, sizeof(ADDRINT));
        st.Address = retAddr;
        st.Value = v;

        stackRets.push_back(st);
    }
}

ADDRINT lst_rtn_st = 0;

VOID rtnExitHandler(ADDRINT ins)
{
    if (entryPassed && stackRets.size() != 0 && ins != lst_rtn_st)
    {
        ADDRINT addr = stackRets.back().Address;
        ADDRINT val;
        
        PIN_SafeCopy(&val, (ADDRINT*)addr, sizeof(ADDRINT));
        if (val != stackRets.back().Value)
        {
            report_so(&stackInfo, &stackRets, ins, val);
        }

        stackRets.pop_back();
        if (!stackInfo.empty())
            stackInfo.pop_back();

        if (stackRets.empty())
            entryPassed = false;

        lst_rtn_st = ins;
    }
}

VOID SM_Image(IMG img, void*)
{
    string iname = IMG_Name(img);
    if (!IsImageInScope(iname))
        return;
    
    ADDRINT base = IMG_LowAddress(img);
    BOOL full = NeedStackTrace();
    BOOL car = IsCAR();
    ADDRINT entry_offset;
    if (IsEntryImage(iname))
    {
        string entry_name;
        EntryPointInfo(entry_name, entry_offset);
        if (entry_offset)
        {
            RTN entry = RTN_FindByAddress(base + entry_offset);
            if (!RTN_Valid(entry))
            {
                // TODO: make a report, not output
                cout << "Invalid entry point was specified by addres " << hexstr(entry_offset) << "\n";
                PIN_ExitApplication(-1);
            }
            entry_offset += base;
        }
        else
        {
            RTN entry = RTN_FindByName(img, entry_name.c_str());
            if (!RTN_Valid(entry))
            {
                // TODO: the same
                cout << "Invalid entry point was specified by name '" << entry_name << "'\n";
                PIN_ExitApplication(-1);
            }
            entry_offset = RTN_Address(entry);
        }
    }
    
    map<ADDRINT, string> rtnRef;
    for (SEC sec = IMG_SecHead(img); SEC_Valid(sec); sec = SEC_Next(sec))
    {
        for (RTN rtn = SEC_RtnHead(sec); RTN_Valid(rtn); rtn = RTN_Next(rtn))
        {
            RTN_Open(rtn);
            if (full)
            {
                rtnNames.push_back(RTN_Name(rtn));
                rtnRef.insert(make_pair(RTN_Address(rtn), RTN_Name(rtn)));
            }

            if (car)
            {
                for (INS ins = RTN_InsHead(rtn); INS_Valid(ins); ins = INS_Next(ins))
                {
                    if (INS_IsDirectCall(ins))
                    {
                        ADDRINT next_addr = INS_NextAddress(ins);
                        ADDRINT call_addr = INS_DirectBranchOrCallTargetAddress(ins);
                        INS_InsertCall(ins,
                            IPOINT_BEFORE, (AFUNPTR)insCallHandler,
                            IARG_ADDRINT, call_addr,
                            IARG_ADDRINT, next_addr,
                            IARG_REG_VALUE, STOP_PTR,
                            IARG_ADDRINT, entry_offset,
                            IARG_END);
                    }
                    else if (INS_IsRet(ins))
                    {
                        INS_InsertCall(ins,
                            IPOINT_BEFORE, (AFUNPTR)insRetHandler,
                            IARG_INST_PTR,
                            IARG_END);
                    }
                }
                RTN_Close(rtn);
                continue;
            }

            /*TODO:
                ���� �������� �������� �� ����� ������� ����� ��������� �����
            */

            ADDRINT offset = RTN_Address(rtn);
            if (full)
            {
                RTN_InsertCall(rtn,
                    IPOINT_BEFORE, (AFUNPTR)rtnEntryHandlerFull,
                    IARG_ADDRINT, offset,
                    IARG_BOOL, offset == entry_offset,
                    IARG_REG_VALUE, STOP_PTR,
                    IARG_RETURN_IP,
                    IARG_END);
            }
            else
            {
                RTN_InsertCall(rtn,
                    IPOINT_BEFORE, (AFUNPTR)rtnEntryHandler,
                    IARG_BOOL, offset == entry_offset,
                    IARG_REG_VALUE, STOP_PTR,
                    IARG_END);
            }

            RTN_InsertCall(rtn,
                IPOINT_AFTER, (AFUNPTR)rtnExitHandler,
                IARG_REG_VALUE, STOP_PTR,
                IARG_END);

            RTN_Close(rtn);
        }
    }

    pass_rtn_names(&rtnRef);
}

VOID GetStack(std::vector<RtnInfo>& si, std::vector<StackTop>& sr)
{
    si = vector<RtnInfo>(stackInfo);
    sr = vector<StackTop>(stackRets);
}