#include "pin.H"
#include "asm/unistd.h"

#include "common.h"
#include "StackManager.h"
#include "ExitHandler.h"
#include "HeapManager.h"
using namespace std;

const string cars_desc = "Using INS level to intercept calls and rets instead of routines from RTN level (used by Stack Manager)";

KNOB<string> knobImagesList(KNOB_MODE_WRITEONCE, "pintool", "ilist", "error", "List of images to analysis");
KNOB<string> knobOutputFile(KNOB_MODE_WRITEONCE, "pintool", "o", "stdout", "File to write report");
KNOB<BOOL> knobStackTrace(KNOB_MODE_WRITEONCE, "pintool", "strace", "", "Is full stack trace needed");
KNOB<BOOL> knobCallsAndRets(KNOB_MODE_WRITEONCE, "pintool", "car", "", cars_desc);

/*
TO DO LIST:
- ��������� ����������������� ����������� ����� � ���������
- �������� POSIX Signals
*/

int main(int argc, char *argv[])
{
    PIN_InitSymbols();
	if (PIN_Init(argc, argv))
		return -1;
    
    if (!InitTool(knobImagesList.Value(),
                  knobOutputFile.Value(),
                  knobStackTrace.Value(),
                  knobCallsAndRets.Value()))
    {
        return -1;   
    }

    // Handling windows exception
    PIN_AddContextChangeFunction(WindowsExceptionHandler, 0);

    // Handling application finish
    PIN_AddFiniFunction(ApplicationFini, 0);

    // Stack management
    IMG_AddInstrumentFunction(SM_Image, 0);

    // Heap Management
     IMG_AddInstrumentFunction(HM_Image, 0);
     INS_AddInstrumentFunction(HM_Instruction, 0);
    // RTN_AddInstrumentFunction(HM_Routine, 0);
    
    // TODO: handling posix signals

	PIN_StartProgram();
	return 0;
}
