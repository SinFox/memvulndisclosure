#include "ExitHandler.h"
#include "report.h"

VOID WindowsExceptionHandler(THREADID threadIndex,
	CONTEXT_CHANGE_REASON reason,
	const CONTEXT* from,
	CONTEXT* to,
	INT32 info,
	void*)
{
	if (reason == CONTEXT_CHANGE_REASON_EXCEPTION)
	{
		report_exception(info, from);
		output_report();
	}
}

// TODO: POSIX signals

VOID ApplicationFini(INT32 code, void*)
{
	report_fini(code);
	output_report();
}