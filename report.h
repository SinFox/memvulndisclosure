#pragma once
#include <stack>
#include <vector>
#include <map>

#include "common.h"

void pass_rtn_names(std::map<ADDRINT, std::string>* names);
bool set_output_file(std::string ofile);
void output_report();
void report_so(std::vector<RtnInfo>* rtnInfo, std::vector<StackTop>* rets, ADDRINT ins, ADDRINT val);
void report_exception(INT32 info, const CONTEXT *ctxt);
void report_fini(INT32 code);
void report_ho(ADDRINT insAddr, ADDRINT memAddr);
void report_df();
void report_uaf(ADDRINT insAddr, ADDRINT memAddr);
void StackToStr(std::vector<RtnInfo>* rtnInfo, std::vector<StackTop>* rets);