#include <string>
#include <sstream>
#include <iostream>
#include <fstream>

#include "report.h"
#include "StackManager.h"
using namespace std;

#ifdef TARGET_IA32E
constexpr REG INS_PTR = REG_RIP;
#else
constexpr REG INS_PTR = REG_EIP;
#endif 

struct RtnInfo
{
	ADDRINT Offset;
	const char* Name;
};

struct StackTop
{
	ADDRINT Address;
	ADDRINT Value;
};

stringstream report;
ofstream fout;
map<ADDRINT, string> rtnNames;
bool output_to_console = true;

void pass_rtn_names(std::map<ADDRINT, string>* names)
{
	rtnNames.insert(names->begin(), names->end());
}

void StackToStr(vector<RtnInfo>* rtnInfo, vector<StackTop>* rets)
{
	report << "[TRACE] First one is most recent call:\n";
	for (INT32 i = rets->size() - 1; i >= 0; i--)
	{
		map<ADDRINT, string>::iterator iter = rtnNames.find(rtnInfo->at(i).Offset);
		string name = iter != rtnNames.end() ? iter->second : "Unknown";
		report << "\t" << hexstr(rtnInfo->at(i).Offset) << ":\t" << name;
		report << "\t-->\t" << hexstr(rets->at(i).Value) << endl;
	}
}

bool set_output_file(string ofile)
{
	if (ofile.compare("stdout"))
	{
		fout.open(ofile.c_str());
		output_to_console = false;
		return fout.good();
	}

	return true;
}

void output_report()
{
	string str_report = report.str();
	if (output_to_console)
	{
		cout << str_report << endl;
	}
	else
	{
		fout << str_report << endl;
		fout.close();
	}
	report.clear();
}

// Application finish report
void report_fini(INT32 code)
{
	report << "[FINI] Application finished with code " << hexstr(code);
}

// Stack Overflow report
void report_so(std::vector<RtnInfo>* rtnInfo, std::vector<StackTop>* rets, ADDRINT ins, ADDRINT val)
{
	report << "[VULN] Return address " << hexstr(rets->back().Value) << " was rewritten with ";
	report << hexstr(val) << " at " << hexstr(ins) << "." << endl;
	if (NeedStackTrace() && rtnInfo->size() == rets->size())
	{
		StackToStr(rtnInfo, rets);
	}
}

void report_df()
{
	report << "[VULN] Double free bug occured" << endl;
	if (NeedStackTrace())
	{
		vector<RtnInfo> a;
		vector<StackTop> b;
		GetStack(a, b);
		StackToStr(&a, &b);
	}
}

void report_uaf(ADDRINT insAddr, ADDRINT memAddr)
{
	report << "[VULN] Use-After-Free bug occured at instruction " << hexstr(insAddr) << " with memory area " << hexstr(memAddr) << endl;
	if (NeedStackTrace())
	{
		vector<RtnInfo> a;
		vector<StackTop> b;
		GetStack(a, b);
		StackToStr(&a, &b);
	}
}

// Heap Overflow report
void report_ho(ADDRINT insAddr, ADDRINT memAddr)
{
	// get_stack()
	if (!insAddr || !memAddr)
		return;
	
	report << "[VULN] Out of band access to " << hexstr(memAddr) << " at " << hexstr(insAddr) << endl;
	if (NeedStackTrace())
	{
		vector<RtnInfo> a;
		vector<StackTop> b;
		GetStack(a, b);
		StackToStr(&a, &b);
	}
}

// Windows exception report
void report_exception(INT32 info, const CONTEXT* ctxt)
{
	report << "[EXCEPTION] Windows exception occured: " << hexstr(info) << endl;
	report << "[CONTEXT] Last occured context:" << endl;
	report << ContextToStr((CONTEXT*)ctxt);
}