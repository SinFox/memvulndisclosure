#include <fstream>
#include <vector>
#include <sstream>
#include <set>
#include <bitset>
#include <iostream>

#include "common.h"
#include "report.h"
using namespace std;

/*
    CONFIG FILE EXAMPLE

# - is a commentary
[IMAGE]
# Shared objects, Dynamicly linked libraries or executables
path/to/image/or/executable
...
path/to/image/or/executable
[ENTRY]
# image in the latter line must be presented in [IMAGE] section
# only one line for this section:
#   one entry point by name or by callable addres (hex)
#   offset = routine_address - image_lowest_address
path/to/image/or/executable NAME $entry_point_routine_name
path/to/image/or/executable ADDR $offset_called

*/

struct RtnInfo
{
    ADDRINT Offset;
    std::string Name;
};

struct StackTop
{
    ADDRINT Address;
    ADDRINT Value;
};

constexpr UINT8 COND_NUM = 2;
constexpr UINT8 STACK_TRACE = 0;
constexpr UINT8 CALLS_AND_RETS = 1;

// Runtime global variables
set<string> images;
string entryImage = "";
string entryPointName = "";
ADDRINT entryPointOffset = 0;
bitset<COND_NUM> conditions;

vector<string> SplitLine(string str, char c)
{
    vector<string> res;
    stringstream ss(str);
    while(getline(ss, str, c))
    {
        res.push_back(str);
    }
    return res;
}

string Join(vector<string> list, string glue)
{
    string result = "";
    for (size_t i = 0; i < list.size() - 1; i++)
    {
        result.append(list[i]);
        result.append(glue);
    }
    result.append(list.back());
    return result;
}

bool InitTool(std::string path, string ofile, bool strace, bool car)
{
    ifstream fin;
    fin.open(path.c_str());
    if (!fin.is_open())
        return false;
    
    int section = -1;
    while (!fin.eof())
    {
        string line;
        getline(fin, line);
        size_t sharp_pos = line.find('#');
        line = line.substr(0, sharp_pos);
        if (line.empty())
            continue;

        if (!line.compare("[IMAGE]"))
        {
            section = 0;
            continue;
        }
        if (!line.compare("[ENTRY]"))
        {
            section = 1;
            continue;
        }
        if (section == 0)
        {
            ifstream test_fin(line.c_str(), ios::binary);
            if (test_fin.good())
            {
                images.insert(line);
                test_fin.close();
            }
            else
            {
                cout << "Could not open image from list:\n'" << line << "'\n";
                return false;
            }
            continue;
        }
        if (section == 1)
        {
            if (entryPointOffset || entryPointName.size())
            {
                cout << "Only one entry point was expected, but two or more are occured\n";
                return false;
            }
            vector<string> splitted = SplitLine(line, ' ');
            string arg = splitted.back();
            splitted.pop_back();
            string act = splitted.back();
            splitted.pop_back();
            string img_name = Join(splitted, " ");

            if (images.find(img_name) == images.end())
            {
                cout << "Entry point image wasn't presented in [IMAGE] section:\n";
                cout << "'" << line << "'" << endl;
                return false;
            }
            if (!act.compare("NAME"))
            {
                entryPointName = arg;
            }
            else if (!act.compare("ADDR"))
            {
                unsigned long long of = (ADDRINT)strtoull(arg.c_str(), nullptr, 16);
                if (of == 0 || of == ULLONG_MAX)
                {
                    cout << "Bad entry point offset value:\n";
                    cout << "'" << arg << "'\n";
                    return false;
                }
                entryPointOffset = (ADDRINT)of;
            }
            else
            {
                cout << "Bad second argument in [ENTRY]:\n";
                cout << "'" << line << "'\n";
                return false;
            }

            entryImage = img_name;
        }
    }

    if (entryPointName.empty() && !entryPointOffset)
    {
        cout << "An entry point was not specified\n";
        return false;
    }

    conditions[STACK_TRACE] = strace;
    conditions[CALLS_AND_RETS] = car;
    return set_output_file(ofile);
}

bool IsImageInScope(std::string iname)
{
    if (images.find(iname) != images.end())
        return true;
    
#ifdef TARGET_LINUX
    char character = '/';
#elif TARGET_WINDOWS
    char character = '\\';
#endif

    // Searching only by name, not full name
    vector<string> splitted = SplitLine(iname, character);
    return images.find(splitted.back()) == images.end();
}

bool NeedStackTrace()
{
    return conditions[STACK_TRACE];
}

bool IsCAR()
{
    return conditions[CALLS_AND_RETS];
}

string ContextToStr(CONTEXT* ctxt)
{
    stringstream res = "";

#ifdef TARGET_IA32E
    res << "\t" << "RAX: " << hexstr(PIN_GetContextReg(ctxt, REG_RAX)) << "\t";
    res << "\t" << "RBX: " << hexstr(PIN_GetContextReg(ctxt, REG_RBX)) << "\t";
    res << "\t" << "RCX: " << hexstr(PIN_GetContextReg(ctxt, REG_RCX)) << "\n";
    res << "\t" << "RDX: " << hexstr(PIN_GetContextReg(ctxt, REG_RDX)) << "\t";
    res << "\t" << "RDI: " << hexstr(PIN_GetContextReg(ctxt, REG_RDI)) << "\t";
    res << "\t" << "RSI: " << hexstr(PIN_GetContextReg(ctxt, REG_RSI)) << "\n";
    res << "\t" << "RSP: " << hexstr(PIN_GetContextReg(ctxt, REG_RSP)) << "\t";
    res << "\t" << "RBP: " << hexstr(PIN_GetContextReg(ctxt, REG_RBP)) << "\t";
    res << "\t" << "RIP: " << hexstr(PIN_GetContextReg(ctxt, REG_RIP)) << "\n";
#elif TARGET_IA32
    res << "\t" << "EAX: " << hexstr(PIN_GetContextReg(ctxt, REG_EAX)) << "\t";
    res << "\t" << "EBX: " << hexstr(PIN_GetContextReg(ctxt, REG_EBX)) << "\t";
    res << "\t" << "ECX: " << hexstr(PIN_GetContextReg(ctxt, REG_ECX)) << "\n";
    res << "\t" << "EDX: " << hexstr(PIN_GetContextReg(ctxt, REG_EDX)) << "\t";
    res << "\t" << "EDI: " << hexstr(PIN_GetContextReg(ctxt, REG_EDI)) << "\t";
    res << "\t" << "ESI: " << hexstr(PIN_GetContextReg(ctxt, REG_ESI)) << "\n";
    res << "\t" << "ESP: " << hexstr(PIN_GetContextReg(ctxt, REG_ESP)) << "\t";
    res << "\t" << "EBP: " << hexstr(PIN_GetContextReg(ctxt, REG_EBP)) << "\t";
    res << "\t" << "EIP: " << hexstr(PIN_GetContextReg(ctxt, REG_EIP)) << "\n";
#endif

    return res.str();
}

void __fastcall EntryPointInfo(string& name, ADDRINT& offset)
{
    offset = entryPointOffset;
    name = entryPointName;
}

bool IsEntryImage(string name)
{
    if (!entryImage.compare(name))
        return true;

#ifdef TARGET_LINUX
    char character = '/';
#elif TARGET_WINDOWS
    char character = '\\';
#endif

    // Searching only by name, not full name
    vector<string> splitted = SplitLine(name, character);
    return !entryImage.compare(splitted.back());
}

// make info
bool IsEntryReached()
{
    return true;
}