#include <string>
#include <map>
#include <iostream>

#include "HeapManager.h"
#include "common.h"
#include "report.h"

#ifdef TARGET_WINDOWS
#define ALLOC_FUNC_NAME "RtlAllocateHeap"
#define FREE_FUNC_NAME "RtlFreeHeap"
#define ALLOC_ARG_NUMBER (ADDRINT)2
#define FREE_ARG_NUMBER (ADDRINT)2
#else
#define ALLOC_FUNC_NAME "malloc"
#define FREE_FUNC_NAME "free"
#define ALLOC_ARG_NUMBER (ADDRINT)0
#define FREE_ARG_NUMBER (ADDRINT)0
#endif

using namespace std;

map<ADDRINT, ADDRINT> allocated;
map<ADDRINT, ADDRINT> freed;
ADDRINT lastRequestedSize = 0;
BOOL entryPassedH = false;

ADDRINT minAllocated = 0,
		maxAllocated = 0,
		minFreed = 0,
		maxFreed = 0;

/*
TODO:

*/

VOID HandleEntry()
{
	cout << "Entry passed" << endl;
	entryPassedH = true;
}

VOID FindMinAndMax(map<ADDRINT, ADDRINT>::iterator begin,
	map<ADDRINT, ADDRINT>::iterator end, ADDRINT& min, ADDRINT &max)
{	
	min = -1;
	max = 0;
	for (map<ADDRINT, ADDRINT>::iterator area = begin; area != end; area++)
	{
		if (area->first < min)
			min = area->first;
		if (area->first + area->second > max)
			max = area->first + area->second;
	}
	
}

VOID AllocationEntry(ADDRINT size)
{
	if (!entryPassedH)
		return;

	lastRequestedSize = size;
}

VOID AllocationExit(ADDRINT addr)
{
	if (!entryPassedH)
		return;
	
	if (!lastRequestedSize)
	{
		if (allocated.find(addr) == allocated.end())
			cout << "RtlAllocateHeap returning was accured without calling: " << hexstr(addr) << endl;

		return;
	}

	map<ADDRINT, ADDRINT>::iterator area = freed.find(addr);
	if (area != freed.end())
	{
		if (lastRequestedSize < area->second)
		{
			ADDRINT newAddr = area->first + lastRequestedSize;
			ADDRINT newSize = area->second - lastRequestedSize;
			freed.erase(area);
			freed.insert(make_pair(newAddr, newSize));
			FindMinAndMax(freed.begin(), freed.end(), minFreed, maxFreed);
		}
		else
		{
			freed.erase(area);
			FindMinAndMax(freed.begin(), freed.end(), minFreed, maxFreed);
		}
	}

	cout << "allocated: " << hexstr(addr) << " -- " << lastRequestedSize << endl;

	// Handle situation when returned used address
	allocated.insert(make_pair(addr, lastRequestedSize));

	if (!minAllocated || minAllocated > addr)
	{
		minAllocated = addr;
	}
	if (!maxAllocated || maxAllocated < addr + lastRequestedSize)
	{
		maxAllocated = addr + lastRequestedSize;
	}

	lastRequestedSize = 0;
}

VOID FreeEntry(ADDRINT addr)
{
	if (!entryPassedH)
		return;

	map<ADDRINT, ADDRINT>::iterator area = freed.find(addr);
	if (area != freed.end())
	{
		// Report double free bug
		report_df();
		return;
	}

	area = allocated.find(addr);
	ADDRINT size;

	if (area != allocated.end())
	{
		cout << "Freed heap area: " << hexstr(addr) << endl;
		freed.insert(make_pair(addr, area->second));
		size = area->second;
		allocated.erase(area);

		FindMinAndMax(allocated.begin(), allocated.end(), minAllocated, maxAllocated);

		if (!minFreed || minFreed > addr)
		{
			minFreed = addr;
		}
		if (!maxFreed || maxFreed < addr + size)
		{
			maxFreed = addr + size;
		}
		
		return;
	}
	else
	{
		cout << "Freed unhandled area " << hexstr(addr) << endl;
		freed.insert(make_pair(addr, UINT32_MAX));
		size = 0;

		if (!minFreed || minFreed > addr)
		{
			minFreed = addr;
		}
		if (!maxFreed || maxFreed < addr)
		{
			maxFreed = addr;
		}

		return;
	}
}

// check return value!!!
VOID RtlFreeExitHandler(ADDRINT succ)
{
	// Empty for a while
}

VOID CheckForHeapUsage(ADDRINT insAddr, ADDRINT faddr, ADDRINT saddr)
{
	if (faddr && faddr >= minAllocated - 10 && faddr <= maxAllocated + 10)
	{
		map<ADDRINT, ADDRINT>::iterator area;
		bool found = false;
		for (area = allocated.begin(); area != allocated.end(); area++)
		{
			if (faddr >= area->first && faddr < area->first + area->second)
			{
				found = true;
				break;
			}
		}

		if (!found)
		{
			// cout << "Heap Buffer overflow at ins " << hexstr(insAddr) << ": " << hexstr(faddr) << endl;
			report_ho(insAddr, faddr);
		}
	}
	else if (faddr && faddr >= minFreed && faddr <= maxFreed)
	{
		report_uaf(insAddr, faddr);
	}

	if (saddr && saddr >= minAllocated - 10 && saddr <= maxAllocated + 10)
	{
		map<ADDRINT, ADDRINT>::iterator area;
		bool found = false;
		for (area = allocated.begin(); area != allocated.end(); area++)
		{
			if (saddr >= area->first && saddr < area->first + area->second)
			{
				found = true;
				break;
			}
		}

		if (!found)
		{
			// cout << "Heap Buffer overflow at ins " << hexstr(insAddr) << ": " << hexstr(saddr) << endl;
			report_ho(insAddr, saddr);
		}
	}
	else if (saddr && saddr >= minFreed && saddr <= maxFreed)
	{
		report_uaf(insAddr, saddr);
	}
}

VOID HandleWrites(ADDRINT insAddr, ADDRINT faddr, ADDRINT saddr)
{
	if (!entryPassedH)
		return;

	CheckForHeapUsage(insAddr, faddr, saddr);
}

VOID HandleReads(ADDRINT insAddr, ADDRINT faddr, ADDRINT saddr)
{
	if (!entryPassedH)
		return;

	CheckForHeapUsage(insAddr, faddr, saddr);
}

VOID HM_Image(IMG img, void*)
{
	string iname = IMG_Name(img);
	if (!IsImageInScope(iname))
		return;

	string entryName = "";
	ADDRINT entryOffset = 0;
	ADDRINT base = IMG_LowAddress(img);
	if (IsEntryImage(iname))
		EntryPointInfo(entryName, entryOffset);

	for (SYM sym = IMG_RegsymHead(img); SYM_Valid(sym); sym = SYM_Next(sym))
	{
		string undFuncName = PIN_UndecorateSymbolName(SYM_Name(sym), UNDECORATION_NAME_ONLY);

		RTN rtn = RTN_FindByAddress(IMG_LowAddress(img) + SYM_Value(sym));
		//  Find the RtlAllocHeap() function.
		if (undFuncName == ALLOC_FUNC_NAME)
		{
			RTN_Open(rtn);
			RTN_InsertCall(rtn,
				IPOINT_BEFORE, AFUNPTR(AllocationEntry),
				IARG_FUNCARG_ENTRYPOINT_VALUE, ALLOC_ARG_NUMBER,
				IARG_END);
			RTN_InsertCall(rtn,
				IPOINT_AFTER, AFUNPTR(AllocationExit),
				IARG_FUNCRET_EXITPOINT_VALUE,
				IARG_END);
			RTN_Close(rtn);
		}
		else if (undFuncName == FREE_FUNC_NAME)
		{
			RTN_Open(rtn);
			RTN_InsertCall(rtn,
				IPOINT_BEFORE, AFUNPTR(FreeEntry),
				IARG_FUNCARG_ENTRYPOINT_VALUE, ALLOC_ARG_NUMBER,
				IARG_END);
			/*RTN_InsertCall(rtn,
				IPOINT_AFTER, AFUNPTR(AllocationExit),
				IARG_FUNCRET_EXITPOINT_VALUE,
				IARG_END);*/
			RTN_Close(rtn);
		}
		else if (!entryName.empty() && undFuncName == entryName)
		{
			RTN_Open(rtn);
			RTN_InsertCall(rtn,
				IPOINT_BEFORE, AFUNPTR(HandleEntry),
				IARG_END);
			RTN_Close(rtn);
		}
		else if (entryOffset &&
			     entryOffset + base >= INS_Address(RTN_InsHead(rtn)) &&
			     entryOffset + base <= INS_Address(RTN_InsTail(rtn)))
		{
			for (INS ins = RTN_InsHead(rtn); INS_Valid(ins); ins = INS_Next(ins))
			{
				if (INS_Address(ins) == entryOffset + base)
				{
					INS_InsertCall(ins,
						IPOINT_BEFORE, AFUNPTR(HandleEntry),
						IARG_END);
				}
			}
		}
	}
}

VOID HM_Instruction(INS ins, void*)
{
	RTN rtn = INS_Rtn(ins);
	SEC sec = RTN_Sec(rtn);
	IMG img = SEC_Img(sec);

	string iname = IMG_Name(img);
	string rname = RTN_Name(rtn);
	if (!IsImageInScope(iname))
		return;

	string n;
	ADDRINT o;
	EntryPointInfo(n, o);
	
	if (n.empty() || rname.compare(n) != 0)
		return;

	if (INS_IsMemoryWrite(ins))
	{
		if (INS_MemoryOperandCount(ins) == 1)
		{
			INS_InsertCall(ins,
				IPOINT_BEFORE, AFUNPTR(HandleWrites),
				IARG_INST_PTR,
				IARG_MEMORYWRITE_EA,
				IARG_ADDRINT, 0,
				IARG_END);
		}
		else if (INS_MemoryOperandCount(ins) == 2 &&
				 INS_MemoryOperandIsWritten(ins, 0) &&
				 INS_MemoryOperandIsWritten(ins, 1))
		{
			INS_InsertCall(ins,
				IPOINT_BEFORE, AFUNPTR(HandleWrites),
				IARG_INST_PTR,
				IARG_MEMORYOP_EA, 0,
				IARG_MEMORYOP_EA, 1,
				IARG_END);
		}
		else if (INS_MemoryOperandCount(ins) == 2 &&
				 !INS_MemoryOperandIsWritten(ins, 0) &&
				 INS_MemoryOperandIsWritten(ins, 1))
		{
			INS_InsertCall(ins,
				IPOINT_BEFORE, AFUNPTR(HandleWrites),
				IARG_INST_PTR,
				IARG_ADDRINT, 0,
				IARG_MEMORYOP_EA, 1,
				IARG_END);
		}
		else if (INS_MemoryOperandCount(ins) == 2 &&
				 INS_MemoryOperandIsWritten(ins, 0) &&
				 !INS_MemoryOperandIsWritten(ins, 1))
		{
			INS_InsertCall(ins,
				IPOINT_BEFORE, AFUNPTR(HandleWrites),
				IARG_INST_PTR,
				IARG_MEMORYOP_EA, 0,
				IARG_ADDRINT, 0,
				IARG_END);
		}
		
	}
	else if (INS_IsMemoryRead(ins))
	{
		if (INS_MemoryOperandCount(ins) == 1)
		{
			INS_InsertCall(ins,
				IPOINT_BEFORE, AFUNPTR(HandleReads),
				IARG_INST_PTR,
				IARG_MEMORYREAD_EA,
				IARG_ADDRINT, 0,
				IARG_END);
		}
		else if (INS_MemoryOperandCount(ins) == 2 &&
			INS_MemoryOperandIsRead(ins, 0) &&
			INS_MemoryOperandIsRead(ins, 1))
		{
			INS_InsertCall(ins,
				IPOINT_BEFORE, AFUNPTR(HandleReads),
				IARG_INST_PTR,
				IARG_MEMORYOP_EA, 0,
				IARG_MEMORYOP_EA, 1,
				IARG_END);
		}
		else if (INS_MemoryOperandCount(ins) == 2 &&
			!INS_MemoryOperandIsRead(ins, 0) &&
			INS_MemoryOperandIsRead(ins, 1))
		{
			INS_InsertCall(ins,
				IPOINT_BEFORE, AFUNPTR(HandleReads),
				IARG_INST_PTR,
				IARG_ADDRINT, 0,
				IARG_MEMORYOP_EA, 1,
				IARG_END);
		}
		else if (INS_MemoryOperandCount(ins) == 2 &&
			INS_MemoryOperandIsRead(ins, 0) &&
			!INS_MemoryOperandIsRead(ins, 1))
		{
			INS_InsertCall(ins,
				IPOINT_BEFORE, AFUNPTR(HandleReads),
				IARG_INST_PTR,
				IARG_MEMORYOP_EA, 0,
				IARG_ADDRINT, 0,
				IARG_END);
		}
	}
}