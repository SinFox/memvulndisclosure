#pragma once
#include <vector>

#include "pin.H"
#include "common.h"

VOID SM_Image(IMG img, void*);

VOID GetStack(std::vector<RtnInfo>& si, std::vector<StackTop>& sr);