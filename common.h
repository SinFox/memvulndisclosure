#include <string>

#include "pin.H"

struct RtnInfo;
struct StackTop;

bool InitTool(std::string path,
			  std::string ofile,
			  bool strace, 
			  bool car);
bool IsImageInScope(std::string iname);
bool NeedStackTrace();
bool IsCAR();
bool IsEntryReached();
std::string ContextToStr(CONTEXT* ctxt);
void EntryPointInfo(std::string& name, ADDRINT& offset);
bool IsEntryImage(std::string name);