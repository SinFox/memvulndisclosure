#pragma once
#include "pin.H"

VOID WindowsExceptionHandler(THREADID threadIndex,
							 CONTEXT_CHANGE_REASON reason,
							 const CONTEXT* from,
							 CONTEXT* to,
							 INT32 info,
							 void*);
VOID ApplicationFini(INT32 code, void*);